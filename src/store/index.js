import { createStore } from 'vuex'
import http from "./http-common";
export default createStore({
  state: {
    userData: {
    
    }
  },
  mutations: {
    set_login_user(state, data) {
      console.log(data)
      state.userData = data;
    },
  },
  getters: {
    getLoginUser(state) {

      return state.userData;
    }
},
  actions: {
    LOGIN_METAMASK: ({commit}, payload) => {
      return new Promise((resolve, reject) => {
        http.post(`auth/signup/metamask`, payload).then((result) => {
          
          commit('set_login_user', result.data);
          resolve(result)
        }).catch((error) => {

          reject(error)
        })
      })
    },
    LOG_OUT: ({commit}) => {
      commit('set_login_user', {});
    },
    CREATE_TRANSACTION: ({},payload) => {
      
      return new Promise((resolve, reject) => {
        http.post(`/transactions/create`, payload).then((result) => {
          
          resolve(result)
        }).catch((error) => {

          reject(error)
        })
      })
    },
    GET_TRANSACTIONS: ({},payload) => {
      console.log(payload)
      return new Promise((resolve, reject) => {
        http.post(`/transactions/get`, payload).then((result) => {
          
          resolve(result)
        }).catch((error) => {

          reject(error)
        })
      })
    },
  },
})
