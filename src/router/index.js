import { createRouter, createWebHistory } from 'vue-router'


const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "about" */ '../views/Home.vue'),
    meta: { requiresAuth: false }
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
    meta: { requiresAuth: false }
  },
  {
    path: '/exchange',
    name: 'Exchange',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Exchange.vue'),
    meta: { requiresAuth: false }
  },
  {
    path: '/login',
    name: 'Login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue'),
    meta: { requiresAuth: false }
  },
  {
    path: '/403',
    name: '403',
    component: () =>
        import ('../views/errors/403.vue'),
    meta: { requiresAuth: false }

},
{
    path: '/404',
    name: '404',
    component: () =>
        import ('../views/errors/404.vue'),
    meta: { requiresAuth: false }

},
{
    path: '/500',
    name: '500',
    component: () =>
        import ('../views/errors/500.vue'),
    meta: { requiresAuth: false }

},
]



const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  // console.log(to)
  // access store via `router.app.$store` here.
  if (!to.matched.length) {
      next('/404');
  } else {
      
      if (to.meta.requiresAuth) {
          
          // if(data) {
          //     store.dispatch('REFRESH_SESSION', data)
          //     next();
          // } else {
              next('/login')
          // }
      } else {
          // if(data) 
          //     store.dispatch('REFRESH_SESSION', data)

          next();
      }
  }
});

export default router
